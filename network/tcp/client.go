package tcp

import (
	"log"
	"net"
	"screenRobot/network"
)

// Connection 连接tcp
func Connection(addr *net.TCPAddr, callBack func(msg string)) {
	conn, err := net.DialTCP("tcp", nil, addr)
	if network.Handler("tcp连接失败", err) {
		return
	}

	buffer := make([]byte, 1024)

	for {
		size, err := conn.Read(buffer)
		if network.Handler("读取信息失败", err) {
			continue
		}
		log.Println("收到信息：", string(buffer[:size]))

		//回调函数处理
		callBack(string(buffer[:size]))
	}
}
