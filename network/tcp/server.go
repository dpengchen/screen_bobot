package tcp

import (
	"log"
	"net"
	"screenRobot/network"
)

var Clients = make([]net.Conn, 0)
var ListenEnable = true
var ClientLen = make(chan int)

// CreateServer 创建服务
func CreateServer() {
	if !ListenEnable {
		return
	}
	addr, err := net.ResolveTCPAddr("tcp", ":9700")
	if network.Handler("解析地址异常", err) {
		return
	}
	conn, err := net.ListenTCP("tcp", addr)
	if network.Handler("监听tcp异常", err) {
		return
	}

	go func() {
		defer conn.Close()
		for {
			if !ListenEnable {
				break
			}
			accept, err := conn.Accept()
			log.Println("连接服务器：", accept.RemoteAddr())
			if network.Handler("连接异常", err) {
				continue
			}
			Clients = append(Clients, accept)
			ClientLen <- len(Clients)
		}
		//释放资源
		for _, v := range Clients {
			v.Close()
		}
	}()

}
