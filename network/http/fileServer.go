package http

import (
	"log"
	"net/http"
)

var server *http.Server
var fs http.Handler

func StartFileServer(path string) {
	fs = http.FileServer(http.Dir(path))
	server = &http.Server{
		Addr:    ":80",
		Handler: http.HandlerFunc(handleFile),
	}
	err := server.ListenAndServe()
	if err != nil {
		log.Println("启动http服务失败：", err)
	}
}

func RestartFileServer(path string) {
	fs = http.FileServer(http.Dir(path))
}

func handleFile(resp http.ResponseWriter, req *http.Request) {
	http.StripPrefix("/", fs).ServeHTTP(resp, req)
}
