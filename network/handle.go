package network

import (
	"log"
	"runtime"
)

// Handler 处理异常
func Handler(msg string, err error) bool {
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		log.Println("错误：", msg, file, line, err)
		return true
	}
	return false
}
