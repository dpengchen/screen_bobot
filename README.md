# ScreenRobot 局域网群控助手

> 功能介绍：
> 1. `http` 静态文件共享服务
> 2. 服务端客户端群控同步功能
>
> 群控功能指的是：我在服务端做任何操作，客户端都会同步进行操作；应用场景可以为，群控执行执行脚本流程

## 常见问题解决：

### 一、 windows下 `c/c++` 的编译问题

下载编译器：

1. 进入网站 [MinGW-w64](https://www.mingw-w64.org/)
2. 找到download中的 builds [打包好的网站](https://github.com/niXman/mingw-builds-binaries/releases)
3. 下载解压，将 `解压路径/bin` 添加到环境变量中
4. 设置 `go` 环境
    ```shell
    go env -w CGO_ENABLED=1
    go env -w 
    # 如果没有报错下面不用执行，需要到下载解压之后的/bin中查找有没有匹配的文件
    go env -w CC=x86_64-w64-mingw32-gcc
    go env -w CXX=x86_64-w64-mingw32-g++
    ```

### 二、 引入 `C` 语言

> 声明：
> 1. 可以使用相对路径、绝对路径 对于要配置的 `CFLAGS`、`LDFLAGS` 都可以使用。
> 2. 编译之后的名字链接库一定要是 `lib + 文件名.a`
> 3. 一定要有头文件 `xxx.h` 文件声明要调用的函数名以及结构体等

```shell
文件结构：
|project
|
|——————lib
|       |——————test.h
|       |——————test.c
|——————main.go

```

```c
//test.h
void test();
void testNumber(int num);

//结构体参数
typedef struct{
   char[10] name;
}Test;
void testStruct(Test* test);

//外部函数，golang中传递过去
void external(char c);
```

```c
//test.c
#include <stdio.h>
#include "test.h"

void test(){
   printf("我执行了");
}
void testNumber(int num){
   printf("我是数字%d",num);
}
void testStruct(Test* test){
   printf("名字：%s",test->name)
}
```

```shell
# 编译test.c
gcc -c test.c
# 链接到test.o
ar rsc libteset.a test.o
```

编译完成后

```shell
文件结构：
|project
|
|——————lib
|       |——————test.h
|       |——————libtest.a
|       |——————test.o
|       |——————test.c
|——————main.go
```

```go
package main

/*
#cgo CFLAGS: -I./lib
#cgo LDFLAGS: -I./lib -ltest
#include "test.h"
*/

import "C"
import "log"

//export External
func External(c C.char) {
   log.Println(c)
}

func main() {
   C.test()
   C.testNumber(C.int(10))
   C.testStruct(&C.Temp{
      name: C.newString("dpengchen"),
   })
}

```