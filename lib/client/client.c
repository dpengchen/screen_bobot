#include <stdio.h>
#include "client.h"
#include <windows.h>

// 键盘按下
void keyDown(int c) {
    // 模拟用户按下键盘某个键
    INPUT ip;
    ip.type = INPUT_KEYBOARD;
    ip.ki.wScan = 0;
    ip.ki.time = 0;
    ip.ki.dwExtraInfo = 0;
    ip.ki.dwFlags = 0; // 0 for key press
    ip.ki.wVk = c;
    SendInput(1, &ip, sizeof(INPUT));
}

// 键盘松开
void keyUp(int c) {
    // 模拟用户松开键盘某个键
    INPUT ip;
    ip.type = INPUT_KEYBOARD;
    ip.ki.wScan = 0;
    ip.ki.time = 0;
    ip.ki.dwExtraInfo = 0;
    ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
    ip.ki.wVk = c;
    SendInput(1, &ip, sizeof(INPUT));
}

// 鼠标移动
void mouseMove(MouseEvent* mouseEvent) {
    // 模拟用户移动鼠标
    SetCursorPos(mouseEvent->x, mouseEvent->y);
    printf("x：%d\ty：%d\tlClick：%d\trClick：%d",mouseEvent->x,mouseEvent->y,mouseEvent->lClick,mouseEvent->rClick);

    // 模拟用户点击鼠标左键
    if (mouseEvent->lClick) {
        mouse_event(MOUSEEVENTF_LEFTDOWN, mouseEvent->x, mouseEvent->y, 0, 0);
        mouse_event(MOUSEEVENTF_LEFTUP, mouseEvent->x, mouseEvent->y, 0, 0);
    }

    // 模拟用户点击鼠标右键
    if (mouseEvent->rClick) {
        mouse_event(MOUSEEVENTF_RIGHTDOWN, mouseEvent->x, mouseEvent->y, 0, 0);
        mouse_event(MOUSEEVENTF_RIGHTUP, mouseEvent->x, mouseEvent->y, 0, 0);
    }

    // 模拟用户双击鼠标左键
    if (mouseEvent->lDBClick) {
        mouse_event(MOUSEEVENTF_LEFTDOWN, mouseEvent->x, mouseEvent->y, 0, 0);
        mouse_event(MOUSEEVENTF_LEFTUP, mouseEvent->x, mouseEvent->y, 0, 0);
        mouse_event(MOUSEEVENTF_LEFTDOWN, mouseEvent->x, mouseEvent->y, 0, 0);
        mouse_event(MOUSEEVENTF_LEFTUP, mouseEvent->x, mouseEvent->y, 0, 0);
    }

    // 模拟用户双击鼠标右键
    if (mouseEvent->rDBClick) {
        mouse_event(MOUSEEVENTF_RIGHTDOWN, mouseEvent->x, mouseEvent->y, 0, 0);
        mouse_event(MOUSEEVENTF_RIGHTUP, mouseEvent->x, mouseEvent->y, 0, 0);
        mouse_event(MOUSEEVENTF_RIGHTDOWN, mouseEvent->x, mouseEvent->y, 0, 0);
        mouse_event(MOUSEEVENTF_RIGHTUP, mouseEvent->x, mouseEvent->y, 0, 0);
    }

    //模拟用户鼠标滚动
    if (mouseEvent->delta){
        mouse_event(MOUSEEVENTF_WHEEL, mouseEvent->x, mouseEvent->y, mouseEvent->delta, 0);
    }
}
