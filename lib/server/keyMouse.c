#include <stdio.h>
#include <windows.h>
#include "keyMouse.h"

//全局钩子变量
HHOOK g_key_hHook;
HHOOK g_mouse_hHook;

// 键盘事件处理函数
LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam) {
    // 检查事件是否需要处理
    if (nCode == HC_ACTION) {
        // 获取键盘事件的信息
        KBDLLHOOKSTRUCT* pkbhs = (KBDLLHOOKSTRUCT*)lParam;
        // 检查按键消息类型
        switch (wParam) {
            // 按键按下
            case WM_KEYDOWN:
            case WM_SYSKEYDOWN:
                // 打印按下的键码
                printf("Key Down: %d\n", pkbhs->vkCode);
                goHandleKey(pkbhs->vkCode,1);
                break;
            // 按键释放
            case WM_KEYUP:
            case WM_SYSKEYUP:
                // 打印释放的键码
                printf("Key Up: %d\n", pkbhs->vkCode);
                goHandleKey(pkbhs->vkCode,0);
                break;
        }
    }
    // 继续传递钩子信息给下一个钩子或目标窗口
    return CallNextHookEx(NULL, nCode, wParam, lParam);
}

/**
鼠标监听
*/
void keyListenerEvent(){

    printf("start keyListenerEvent");

    // 设置键盘钩子，捕获全局键盘事件
    g_key_hHook = SetWindowsHookEx(WH_KEYBOARD_LL, KeyboardProc, NULL, 0);
    if (g_key_hHook == NULL) {
        printf("Failed to set keyboard hook!\n");
        return;
    }

    // 消息循环，保持程序运行
    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0) != 0) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    // 卸载钩子
    UnhookWindowsHookEx(g_key_hHook);
}

// 鼠标事件处理函数
LRESULT CALLBACK MouseProc(int nCode, WPARAM wParam, LPARAM lParam) {
    // 检查事件是否需要处理
    if (nCode == HC_ACTION) {
        // 获取鼠标事件的信息
        //MOUSEHOOKSTRUCT* pmhs = (MOUSEHOOKSTRUCT*)lParam;
        MSLLHOOKSTRUCT* pmhs = (MSLLHOOKSTRUCT*)lParam;
        printf("wParam type :%d",wParam);
        // 检查消息类型
        switch (wParam) {
            // 鼠标移动
            case WM_MOUSEMOVE:
                // 打印鼠标位置
                printf("Mouse Move: x=%d, y=%d\n", pmhs->pt.x, pmhs->pt.y);
                goHandleMouse(pmhs->pt.x,pmhs->pt.y,0,0,0,0,0);
                break;
            // 左键按下
            case WM_LBUTTONDOWN:
                printf("Left Mouse Button Down: x=%d, y=%d\n", pmhs->pt.x, pmhs->pt.y);
                goHandleMouse(pmhs->pt.x,pmhs->pt.y,1,0,0,0,0);
                break;
            // 左键释放
            case WM_LBUTTONUP:
                printf("Left Mouse Button Up: x=%d, y=%d\n", pmhs->pt.x, pmhs->pt.y);
                goHandleMouse(pmhs->pt.x,pmhs->pt.y,0,0,0,0,0);
                break;
            // 左键双击
            case WM_LBUTTONDBLCLK:
                printf("Left Mouse Button Double Click: x=%d, y=%d\n", pmhs->pt.x, pmhs->pt.y);
                goHandleMouse(pmhs->pt.x,pmhs->pt.y,0,0,1,0,0);
                break;
            // 右键按下
            case WM_RBUTTONDOWN:
                printf("Right Mouse Button Down: x=%d, y=%d\n", pmhs->pt.x, pmhs->pt.y);
                goHandleMouse(pmhs->pt.x,pmhs->pt.y,0,1,0,0,0);
                break;
            // 右键释放
            case WM_RBUTTONUP:
                printf("Right Mouse Button Up: x=%d, y=%d\n", pmhs->pt.x, pmhs->pt.y);
                goHandleMouse(pmhs->pt.x,pmhs->pt.y,0,0,0,0,0);
                break;
            // 右键双击
            case WM_RBUTTONDBLCLK:
                printf("Right Mouse Button Double Click: x=%d, y=%d\n", pmhs->pt.x, pmhs->pt.y);
                goHandleMouse(pmhs->pt.x,pmhs->pt.y,0,0,0,1,0);
                break;
            //鼠标滚动
            case WM_MOUSEWHEEL:
                int delte = GET_WHEEL_DELTA_WPARAM(pmhs->mouseData);
                printf("Mouse wheel move %d",delte);
                goHandleMouse(pmhs->pt.x,pmhs->pt.y,0,0,0,0,delte);
                break;
        }
    }
    // 继续传递钩子信息给下一个钩子或目标窗口
    return CallNextHookEx(NULL, nCode, wParam, lParam);
}

void mouseListenerEvent(){
    printf("start mouseListenerEvent");

    g_mouse_hHook = SetWindowsHookEx(WH_MOUSE_LL,MouseProc,NULL,0);
    if (g_mouse_hHook == NULL){
        printf("Failed to set keyboard hook!\n");
        return;
    }

    // 消息循环，保持程序运行
    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0) != 0) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    // 卸载钩子
    UnhookWindowsHookEx(g_mouse_hHook);
}