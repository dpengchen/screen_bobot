package core

/*
#cgo CFLAGS: -I../../lib/client
#cgo LDFLAGS: -L../../lib/client -lclient
#include "client.h"
*/
import "C"
import (
	"screenRobot/server/core"
)

// MouseHandle 处理信息
func MouseHandle(mouse core.KeyMouse) {
	cMouseEvent := C.MouseEvent{
		x:        C.int(mouse.X),
		y:        C.int(mouse.Y),
		lClick:   C.int(mouse.LClick),
		rClick:   C.int(mouse.RClick),
		lDBClick: C.int(mouse.LDBClick),
		rDBClick: C.int(mouse.RDBClick),
		delta:    C.int(mouse.Delta),
	}

	C.mouseMove(&cMouseEvent)
}

// KeyDown 按键按下
func KeyDown(key int) {
	if key != -1 {
		C.keyDown(C.int(key))
	}
}

// KeyUp 按键抬起
func KeyUp(key int) {
	if key != -1 {
		C.keyUp(C.int(key))
	}
}
